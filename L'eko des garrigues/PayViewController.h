//
//  PayViewController.h
//  L'eko des garrigues
//
//  Created by boris on 03/07/2014.
//  Copyright (c) 2014 Wearcraft. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PayPalMobile/PayPalMobile.h"

//@interface PayViewController : UIViewController <PayPalPaymentDelegate, PayPalFuturePaymentDelegate, UIPopoverControllerDelegate, UITextFieldDelegate>
@interface PayViewController : UIViewController <UIPopoverControllerDelegate>

//@property(nonatomic, strong, readwrite) NSString *environment;
//@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;
//@property(nonatomic, strong, readwrite) NSString *resultText;


@end

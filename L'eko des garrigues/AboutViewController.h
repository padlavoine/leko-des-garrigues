//
//  AboutViewController.h
//  L'eko des garrigues
//
//  Created by boris on 03/07/2014.
//  Copyright (c) 2014 Wearcraft. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <FacebookSDK/FacebookSDK.h>

@interface AboutViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *aboutText;



@end

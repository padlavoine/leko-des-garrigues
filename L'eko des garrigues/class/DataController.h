//
//  HistoricalTracks.h
//  L'eko des garrigues
//
//  Created by Boris WEARCRAFT on 04/03/2016.
//  Copyright © 2016 Wearcraft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface DataController : NSObject

@property (strong) NSManagedObjectContext *managedObjectContext;

@end

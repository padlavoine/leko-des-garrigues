//
//  PLAYLIST+CoreDataProperties.m
//  L'eko des garrigues
//
//  Created by Boris WEARCRAFT on 03/05/2016.
//  Copyright © 2016 Wearcraft. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PLAYLIST+CoreDataProperties.h"

@implementation PLAYLIST (CoreDataProperties)

@dynamic trackname;
@dynamic trackartist;
@dynamic date;

@end
